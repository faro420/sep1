package uut;
/**
 * @author: Mir Farshid Baha
 */
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestClass {
    private OrderSelector order;
    
    @Before
    public void init(){
        order = new OrderSelector();
    }
    
    @Test
    public void testType1A() {
        // Typ 1, 0 < x <= 30
        assertEquals("The result must be 4001001", 4001001, order.order(25, 1));
        assertEquals("The result must be 4001001", 4001001, order.order(30, 1));

    }

    @Test
    public void testType1B() {
        // Typ 1, 30 < x <= 100
        assertEquals("The result must be 4001006", 4001006, order.order(60, 1));
        assertEquals("The result must be 4001006", 4001006, order.order(100, 1));
    }

    @Test
    public void testType1C() {
        // Typ 1, 100 < x <= 238
        assertEquals("The result must be 4001011", 4001011, order.order(203, 1));
        assertEquals("The result must be 4001011", 4001011, order.order(238, 1));

    }

    @Test
    public void testType2A() {
        // Typ 2, 0 < x <= 41
        assertEquals("The result must be 4002001", 4002001, order.order(30, 2));
        assertEquals("The result must be 4002001", 4002001, order.order(41, 2));
    }

    @Test
    public void testType2B() {
        // Typ 2, 41 < x <= 99
        assertEquals("The result must be 4001204", 4001204, order.order(77, 2));
        assertEquals("The result must be 4001204", 4001204, order.order(99, 2));

    }

    @Test
    public void testType2C() {
        // Typ 2, 99 < x <= 401
        assertEquals("The result must be 4002381", 4002381, order.order(300, 2));
        assertEquals("The result must be 4002381", 4002381, order.order(401, 2));
    }
}
