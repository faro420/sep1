package pointSpaceObserved;
/**
 * Interface for the Observer-Pattern
 * @author Thomas Lehmann
 */
public interface Observer {
    // Method is called to inform on a change in the observed subject.
    // public abstract void notifyOfChange();
    void notifyOfChange();
}
