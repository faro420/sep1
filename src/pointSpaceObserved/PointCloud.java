package pointSpaceObserved;

public class PointCloud extends PointList {
  private int centreOfMassX;
  private int centreOfMassY;
  
//  public void fixValues(){
//	  int temp = 0;
//	  for( int i = 0; i<size(); i++){
//		  Point2D tempPoint = get(i);
//		  temp = temp + tempPoint.getX();
//	  }
//	  centreOfMassX = temp/size();
//	  temp = 0;
//	  for( int i = 0; i<size(); i++){
//		  Point2D tempPoint = get(i);
//		  temp = temp + tempPoint.getY();
//	  }
//	  centreOfMassY = temp/size();	  
//  }
  
    public void fixPoint() {
        int temp1 = 0, temp2 = 0;
        for (int i = 0; i < size(); i++) {
            Point2D tempPoint = get(i);
            temp1 = temp1 + tempPoint.getX();
            temp2 = temp2 + tempPoint.getY();
        }
        centreOfMassX = temp1 / size();
        centreOfMassY = temp2 / size();
    }
    
    
	public void addPoint(Point2D point){
		super.addPoint(point);
		fixPoint();
	}

	public void notifyOfChange() {
		super.notifyOfChange();
		fixPoint();
	}
	public int getCentreOfMassX() {
		return centreOfMassX;
	}
	public int getCentreOfMassY() {
		return centreOfMassY;
	}
//	public int getWayTo(Point2D first, Point2D second){
//		int dx = first.getX()-second.getX();
//		return (int)Math.sqrt(dx*dx+(first.getY()-second.getY())*(first.getY()-second.getY()));
//	}
    public int getWayTo(Point2D first, Point2D second) {
        int dx = first.getX() - second.getX();
        int dy = first.getY() - second.getY();
        return (int) Math.sqrt(dx * dx + dy * dy);
    }

}
