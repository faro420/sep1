package pointSpaceObserved;

import java.util.ArrayList;

/**
 * Class handles a list of points. Multiple points with same coordinate can be
 * added to the list. Assumption: Many points are added. The maximum distance is
 * queried fare more times in comparison to the removal of a point or changes of
 * their values.
 * 
 * @author Thomas Lehmann
 * @version 0.1
 */
public class PointList implements Observer {
    private ArrayList<Point2D> points;
    private int maxDistanceToOrigin;

    /**
     * Default constructor.
     */
    public PointList() {
        points = new ArrayList<Point2D>();
        maxDistanceToOrigin = 0;
    }

    /**
     * Add a a point to the list.
     * 
     * @param point
     *            point to be added to the list.
     */
    public void addPoint(Point2D point) {
        points.add(point);
        point.addObserver(this); // register for changes
        maxDistanceToOrigin = Math.max(maxDistanceToOrigin, point.getDistanceToZero());
    }

    /**
     * @return the maximum distance to origin of all points.
     */
    public int getMaxDistanceToOrigin() {
        return maxDistanceToOrigin;
    }

    /**
     * Remove a point with the given coordinates from the list.
     * 
     * @param point
     *            Coordinates to remove
     */
    // public void removePoint(Point2D point){
    // int index = points.indexOf(point);
    // if(index > -1 ){
    // Point2D pointToRemove = points.get(index);
    // pointToRemove.deleteObserver(this);
    // points.remove(pointToRemove);
    //
    // // success, point removed, so need to recalculate.
    // maxDistanceToOrigin = 0;
    // for( int i = 0 ; i < size() ; i++ ){
    // Point2D p = get( i );
    // maxDistanceToOrigin = Math.max(maxDistanceToOrigin, p.getLängeZuZero());
    // }
    // }
    // }
    public void removePoint(Point2D point) {
        int index = points.indexOf(point);
        if (index > -1) {
            Point2D pointToRemove = points.get(index);
            pointToRemove.deleteObserver(this);
            points.remove(pointToRemove);

            // success, point removed, so need to recalculate.
            calculateMaxDistanceToOrigin();
        }
    }

    /**
     * Calculate the maximum distance to origin over all points in the list.
     */
    private void calculateMaxDistanceToOrigin() {
        maxDistanceToOrigin = 0;
        for (Point2D point : points) {
            maxDistanceToOrigin = Math.max(maxDistanceToOrigin, point.getDistanceToZero());
        }
    }

    /**
     * @return Returns the number of elements in the list.
     */
    public int size() {
        return points.size();
    }

    /**
     * Returns the element at the specified position.
     * 
     * @param index
     *            of the element to return.
     * @return the element at the specified position in this list.
     */
    public Point2D get(int index) {
        return points.get(index);
    }

    // public void notifyOfChange() {
    // // one point in the list has changed, so recalculate.
    // maxDistanceToOrigin = 0;
    // for( Point2D x : points ){
    // int dist = x.getLängeZuZero();
    // maxDistanceToOrigin = Math.max(maxDistanceToOrigin, dist);
    // }
    // }
    public void notifyOfChange() {
        // one point in the list has changed, so recalculate.
        calculateMaxDistanceToOrigin();
    }
}
