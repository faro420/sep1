/**
 * Class represents points in a 2-dimensional plane.
 * Coordinates are integer numbers.
 * 
 * @author Thomas Lehmann
 * @version 0.1 05-05-2009
 */
package pointSpaceObserved;

public class Point2D extends ObserveableSubject implements Cloneable{
	private int x;
	private int y;

	/**
	 * Constructor for 2-dimensional point.
	 * @param x x-coordinate in plane
	 * @param y y-coordinate in plane
	 */
	public Point2D(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	/**
	 * @param x the new x-coordinate
	 */
	public void setX(int x) {
		this.x = x;
		//setChanged();
		notifyObservers();
	}
	/**
	 * @param y the new y-coordinate
	 */
//	public void setY(int y) {
//		this.y = y;
//		//setChanged();
//		notifyObservers();
//	}
	   public void setY(int y) {
	        this.y = y;
	        notifyObservers();
	    }
	/**
	 * @return the x-coordinate
	 */
	public int getX() {
		return x;
	}
	/**
	 * @return the y-coordinate
	 */
	public int getY() {
		return y;
	}
	/**
	 * @return the Cartesian distance to origin.
	 * 
	 */

//	public int getLängeZuZero(){
//		return (int)Math.sqrt(x*x+y*y);
//	}
	   public int getDistanceToZero(){
	        return (int)Math.sqrt(x*x+y*y);
	    }
	/**
	 * Override equals method of Object.
	 */
//	@Override
//	public boolean equals(Object obj) {
//		if(obj instanceof Point2D){
//			if( (this.x == ((Point2D)obj).x) && (this.y == ((Point2D)obj).y) ){
//				return true;
//			}
//			else {
//				return false;
//			}
//		}
//		else{
//			return super.equals(obj);
//		}
//	}
	 @Override
    public boolean equals(Object obj) {
        if (obj instanceof Point2D) {
            return (this.x == ((Point2D) obj).x) && (this.y == ((Point2D) obj).y);
        }
        return false;
    }
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
