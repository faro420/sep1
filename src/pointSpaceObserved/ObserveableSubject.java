package pointSpaceObserved;

import java.util.ArrayList;
/**
 * Base class for the observer-pattern
 * @author Thomas Lehmann
 *
 */
public class ObserveableSubject {
	private ArrayList<Observer> observers; 
	
	public ObserveableSubject() {
		observers = new ArrayList<Observer>();
	}
	public void addObserver(Observer observer){
		observers.add(observer);
	}
	public void deleteObserver(Observer observer){
		observers.remove(observer);
	}
	protected void notifyObservers(){
		for( Observer o : observers ){
			o.notifyOfChange();
		}
	}
}
